const { gql } = require('apollo-server-lambda');

const typeDefs = gql`
    type Member {
        name: String!
    }
    type Team {
        id: String!,
        members: [Member]!
    }
    type Query {
        pickVolunteer(teamId: String!): Member
    }
    type Mutation {
        createTeam(teamdId: String!): Team
        addMember(teamId: String!, memberName: String!): Team
        removeMember(teamId: String!, memberName: String!): Team
    }
`;

module.exports = typeDefs;