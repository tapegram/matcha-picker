"use strict";
const { ApolloServer } = require('apollo-server-lambda');

const typeDefs = require('./schema');
const { fetchTeam, putTeam } = require('./data');

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

const _pickVolunteer = async (teamId) => {
    const team = await fetchTeam(teamId)
    return shuffle(team.members)[0]
}

const resolvers = {
    Query: {
        pickVolunteer: async (parent, args, context, info) => {
            const volunteer = await _pickVolunteer(args.teamId)
            return {name: volunteer.name}
        }
    },
    Mutation: {
        createTeam: async (parent, args, context, info) => {
            const team = {
                id: args.teamdId,
                members: []
            }
            await putTeam(team)
            return team
        },
        addMember: async (parent, args, context, info) => {
            const team = await fetchTeam(args.teamId)
            const updatedTeam = {
                id: team.id,
                members: [...team.members, { name: args.memberName }]
            }
            await putTeam(updatedTeam)
            return updatedTeam
        },
        removeMember: async (parent, args, context, info) => {
            const team = await fetchTeam(args.teamId)
            const updatedTeam = {
                id: team.id,
                members: team.members.filter(member => member.name != args.memberName)
            }
            await putTeam(updatedTeam)
            return updatedTeam
        }
    }
}

const server = new ApolloServer({ 
    typeDefs, 
    resolvers,
    context: ({ event, context }) => ({
        headers: event.headers,
        functionName: context.functionName,
        event,
        context,
    }),
});
exports.graphqlHandler = server.createHandler();